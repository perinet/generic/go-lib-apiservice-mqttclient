/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mqttclient

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
	"gitlab.com/perinet/generic/apiservice/mqttclient/dataModel"
	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/httpserver/periHttp"
	"gitlab.com/perinet/generic/lib/httpserver/rbac"
	"gitlab.com/perinet/generic/lib/utils/filestorage"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
)

const (
	API_VERSION          = "24"
	MQTT_WAIT_WORK       = 250 // time(ms) to wait while processing mqtt work
	CONFIG_FILE_PATH     = "mqttclient/config.json"
	SUBSCRIBEMESSAGEPATH = "/mqtt_client/subscribe/"
)

// URL endpoints definition
func PathsGet() []httpserver.PathInfo {
	return []httpserver.PathInfo{
		{Url: "/mqtt_client", Method: httpserver.GET, Role: rbac.NONE, Call: MQTTClient_Info_Get},
		{Url: "/mqtt_client/status", Method: httpserver.GET, Role: rbac.USER, Call: MQTTClient_Status_Get},
		{Url: "/mqtt_client/config", Method: httpserver.GET, Role: rbac.ADMIN, Call: MQTTClient_Config_Get},
		{Url: "/mqtt_client/config", Method: httpserver.PATCH, Role: rbac.ADMIN, Call: MQTTClient_Config_Set},
		{Url: "/mqtt_client/publish", Method: httpserver.POST, Role: rbac.INTERNAL, Call: MQTTClient_Publish_Set},
		{Url: "/mqtt_client/subscribe", Method: httpserver.POST, Role: rbac.INTERNAL, Call: MQTTClient_Subscribe_Set},
		{Url: "/mqtt_client/subscribe", Method: httpserver.DELETE, Role: rbac.INTERNAL, Call: MQTTClient_Subscribe_Delete},
		{Url: SUBSCRIBEMESSAGEPATH, UrlIsPrefix: true, Method: httpserver.SSE, Role: rbac.INTERNAL},
	}
}

type MQTTClient = dataModel.MQTTClient
type MQTTClientConfig = dataModel.MQTTClientConfig
type MQTTClientStatus = dataModel.MQTTClientStatus
type MQTTClientPublish = dataModel.MQTTClientPublish
type MQTTClientSubscribe = dataModel.MQTTClientSubscribe

// initial variables
var (
	mqttClient MQTTClient = MQTTClient{
		ApiVersion: API_VERSION,
		Config: MQTTClientConfig{
			BrokerURI: "",
			Disabled:  false,
		},
		Status:      dataModel.MQTTClientStatus{},
		ErrorStatus: nodeErrorStatus.NO_ERROR,
	}
	mqttClientConnection        mqtt.Client
	mqttClientConnectionOptions mqtt.ClientOptions                         = *mqtt.NewClientOptions()
	subscriber                  map[uuid.UUID]MQTTClientSubscriberEndpoint = make(map[uuid.UUID]MQTTClientSubscriberEndpoint)
	logger                      log.Logger                                 = *log.Default()
)

// init apiservice
func init() {
	logger.SetPrefix("mqttclient: ")
	// load config file
	filestorage.LoadObject(CONFIG_FILE_PATH, &mqttClient.Config)
	// connect to the broker
	if !mqttClient.Config.Disabled {
		initConnection()
	}
	node.RegisterNodeEventCallback(Reconnect)
	security.RegisterSecurityEventCallback(Reconnect)
}

func Reconnect() {
	reinitConnection()
}

// MQTTClient_Info_Get: returns info object
func MQTTClient_Info_Get(p periHttp.PeriHttp) {
	// prepare the json to be sent with the info of the object
	binaryJson, err := json.Marshal(mqttClient)

	// not possible to marshall the object send an internal error
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// if everthing goes well send response with the json read and status OK
	p.JsonResponse(http.StatusOK, binaryJson)
}

// MQTTClient_Status_Get: returns status object
func MQTTClient_Status_Get(p periHttp.PeriHttp) {
	// prepare the json to be sent with the info of the object
	binaryJson, err := json.Marshal(mqttClient.Status)

	// not possible to marshall the object send an internal error
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// if everthing goes well send response with the json read and status OK
	p.JsonResponse(http.StatusOK, binaryJson)
}

// MQTTClient_Config_Get: returns config object
func MQTTClient_Config_Get(p periHttp.PeriHttp) {

	binaryJson, err := json.Marshal(mqttClient.Config)

	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}
	p.JsonResponse(http.StatusOK, binaryJson)
}

// MQTTClient_Config_Set: handle patch request to update the config
func MQTTClient_Config_Set(p periHttp.PeriHttp) {
	// read tha payload received in request
	payload, _ := p.ReadBody()

	// copy to a temp the actual MQTTClientConfig
	var mqttClientConfig_tmp MQTTClientConfig = MQTTClientConfig{}

	// unmarshall the payload into the temp config and return error if it doesn't work
	err := json.Unmarshal(payload, &mqttClientConfig_tmp)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	type connect_state int
	const (
		NULL       connect_state = 0
		DISCONNECT connect_state = 1
		CONNECT    connect_state = 2
		DISABLE    connect_state = 3
	)

	new_connection_state := NULL

	if mqttClient.Config.Disabled != mqttClientConfig_tmp.Disabled {
		if mqttClientConfig_tmp.Disabled {
			new_connection_state = DISABLE
		} else {
			new_connection_state = CONNECT
		}
	}

	// the broker has changed
	if mqttClient.Status.State == dataModel.CONNECTED && mqttClient.Config.BrokerURI != mqttClientConfig_tmp.BrokerURI {
		new_connection_state = DISCONNECT
	}

	// when everything goes well set the config with the new received
	mqttClient.Config = mqttClientConfig_tmp

	// update the config file
	err = filestorage.StoreObject(CONFIG_FILE_PATH, mqttClient.Config)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// change connection state only if storing new config was successful
	if new_connection_state == CONNECT {
		logger.Println("new connection state: connect")
		if mqttClientConnection != nil {
			mqttClient.Status.State = dataModel.DISCONNECTED // for now the state is diconnected
			openConnection(&mqttClientConnection, &mqttClientConnectionOptions)
			logger.Println("trying to connect again")
		} else {
			mqttClient.Status.State = dataModel.DISCONNECTED
			initConnection()
		}
	} else if new_connection_state == DISCONNECT {
		logger.Println("new connection state: disconnect")
		mqttClientConnection.Disconnect(MQTT_WAIT_WORK)
		mqttClient.Status.State = dataModel.DISCONNECTED
		mqttClient.Status.BrokerURI = ""
		mqttClient.Status.SecurityLevel = dataModel.UNSECURED
	} else if new_connection_state == DISABLE {
		logger.Println("new connection state: disable")
		mqttClientConnection.Disconnect(MQTT_WAIT_WORK)
		mqttClient.Status.State = dataModel.DISABLED
		mqttClient.Status.BrokerURI = ""
		mqttClient.Status.SecurityLevel = dataModel.UNSECURED
	}

	// everything ok, update the status to NO CONTENT
	p.EmptyResponse(http.StatusNoContent)
}

func MQTTClient_Publish_Set(p periHttp.PeriHttp) {
	payload, err := p.ReadBody()
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	var mqttClientPublish MQTTClientPublish = MQTTClientPublish{}
	err = json.Unmarshal(payload, &mqttClientPublish)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	ok := Publish(mqttClientPublish.Topic, mqttClientPublish.Message)

	if ok {
		p.EmptyResponse(http.StatusNoContent)
	} else {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "error on message publish"}`))
	}
}

func send_message_sse(uuid uuid.UUID, message MQTTClientSubscriberData) {
	jsonMessage, err := json.Marshal(message)
	if err != nil {
		logger.Println("send_user_sse_status marshal error: ", err)
		return
	}
	httpserver.SendSSE(SUBSCRIBEMESSAGEPATH+uuid.String(), jsonMessage)
}

func MQTTClient_Subscribe_Set(p periHttp.PeriHttp) {
	payload, err := p.ReadBody()
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	var mqttClientSubscribe MQTTClientSubscribe = MQTTClientSubscribe{}
	err = json.Unmarshal(payload, &mqttClientSubscribe)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	// if subscription for topic already exist, return uuid
	for id, sub := range subscriber {
		if sub.Topic == mqttClientSubscribe.Topic {
			p.JsonResponse(http.StatusOK, id)
			return
		}
	}

	uuid := uuid.New()

	subscriberEndpoint := NewSubscriberEndpoint(mqttClientSubscribe.Topic, func(c mqtt.Client, m mqtt.Message) {
		// log.Println("message received: ", uuid, m.Topic(), string(m.Payload()))
		send_message_sse(uuid, MQTTClientSubscriberData{Topic: m.Topic(), Message: string(m.Payload())})
	})
	subscriber[uuid] = subscriberEndpoint

	p.JsonResponse(http.StatusOK, uuid)
}

func MQTTClient_Subscribe_Delete(p periHttp.PeriHttp) {
	payload, err := p.ReadBody()
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	uuid := uuid.Nil
	err = json.Unmarshal(payload, &uuid)
	if err != nil {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "`+err.Error()+`"}`))
		return
	}

	sub := subscriber[uuid]
	ok := removeSubscriberEndpoint(&sub)

	if !ok {
		p.JsonResponse(http.StatusInternalServerError, json.RawMessage(`{"error": "error on remove publish subscriber"}`))
		return
	}

	delete(subscriber, uuid)

	p.EmptyResponse(http.StatusNoContent)
}

func updateNodeStatus(newNodeStatus node.NodeErrorStatus, reasons ...string) {
	printReasons := func() {
		if len(reasons) > 0 {
			logger.Print(strings.Join(reasons, " "))
		}
	}
	if mqttClient.ErrorStatus < newNodeStatus {
		logger.Println("Update NodeErrorStatus " + mqttClient.ErrorStatus.String() + " -> " + newNodeStatus.String())
		printReasons()
		mqttClient.ErrorStatus = newNodeStatus
		data, _ := json.Marshal(mqttClient.ErrorStatus)
		intHttp.Put(node.ErrorStatusSet, data, nil)
	} else {
		logger.Println("Update NodeErrorStatus supressed " + newNodeStatus.String())
		printReasons()
	}
}
