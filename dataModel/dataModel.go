/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package dataModel

import (
	"encoding/json"

	"gitlab.com/perinet/periMICA-container/apiservice/node"
)

// enum State definition
type MQTTClientState string

const (
	DISABLED     MQTTClientState = "disabled"
	SEARCHING    MQTTClientState = "searching"
	DISCONNECTED MQTTClientState = "disconnected"
	CONNECTING   MQTTClientState = "connecting"
	CONNECTED    MQTTClientState = "connected"
	FAILED       MQTTClientState = "failed"
)

type SecurityLevel string

const (
	UNSECURED     SecurityLevel = "UNSECURED"
	ENCRYPTED     SecurityLevel = "ENCRYPTED"
	AUTHENTICATED SecurityLevel = "AUTHENTICATED"
	AUTHORIZED    SecurityLevel = "AUTHORIZED"
)

// enum Error definition
type Error string

const (
	SERVER_AUTHENTICATION_FAILED Error = "server-authentication-failed"
	CLIENT_AUTHENTICATION_FAILED Error = "client-authentication-failed"
	TIMEOUT_BROKER_DISCOVERY     Error = "timeout-broker-discovery"
	EMPTY                        Error = ""
)

// Struct definition of MQTTClient
type MQTTClient struct {
	ApiVersion  json.Number          `json:"api_version"`
	Config      MQTTClientConfig     `json:"config"`
	Status      MQTTClientStatus     `json:"status"`
	ErrorStatus node.NodeErrorStatus `json:"error_status"`
}

// Definition of MQTTClientConfig
type MQTTClientConfig struct {
	BrokerURI string `json:"broker_uri"`
	Disabled  bool   `json:"disabled"`
}

// Definition of MQTTClientStatus
type MQTTClientStatus struct {
	State         MQTTClientState `json:"state"`
	SecurityLevel SecurityLevel   `json:"security_level"`
	BrokerURI     string          `json:"broker_uri"`
	FailCount     int        	  `json:"fail_count"`
}

// Definition of data type: MQTTClientLog
type MQTTClientLog struct {
	State MQTTClientState `json:"state"`
	Error string          `json:"error"`
}

// Definition of MQTTClientPublish
type MQTTClientPublish struct {
	Topic   string `json:"topic"`
	Message string `json:"message"`
}

// Definition of MQTTClientSubscribe
type MQTTClientSubscribe struct {
	Topic string `json:"topic"`
}
