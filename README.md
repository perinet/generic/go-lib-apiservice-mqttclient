# go-li-apiservice-mqttclient

MQTTClient implementation of [unified api] (https://gitlab.com/perinet/unified-api)

# Example of usage

```
// Example to use the mqttclient
func Example_of_usage(t *testing.T) {

	// add subscriber
	mqttClientSubscriberEndpoint_test := NewSubscriberEndpoint(mqttClientSubscribe.Topic, func(c mqtt.Client, m mqtt.Message) {
		log.Println("message received: ", m.Topic(), string(m.Payload()))
	})
	
	// publish a message
	Publish("testing1/topic", "message 1")
}

```