/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mqttclient

import (
	"encoding/json"
	"testing"

	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gotest.tools/v3/assert"
)

func TestMQTTClientInfo(t *testing.T) {
	var info MQTTClient
	data := intHttp.Get(MQTTClient_Info_Get, nil)
	err := json.Unmarshal(data, &info)

	assert.NilError(t, err)
	assert.Assert(t, info.ApiVersion == "24")
	// TODO check all fields
}
