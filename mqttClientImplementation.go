/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mqttclient

import (
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/perinet/generic/apiservice/mqttclient/dataModel"
)

var (
	subscriberEndpoints []MQTTClientSubscriberEndpoint
)

type MQTTClientSubscriberData struct {
	Topic   string
	Message string
}

// Definition of data type: MQTTClientSubscriberEndpoint
type MQTTClientSubscriberEndpoint struct {
	Topic          string
	MessageHandler mqtt.MessageHandler
}

// Publish messages to the publisher endpoint
func Publish(topic string, message string) bool {
	if mqttClient.Status.State == dataModel.CONNECTED {
		token := mqttClientConnection.Publish(topic, 0, false, message)
		token.Wait()
		return (token.Error() == nil)
	}
	return false
}

// NewSubscriberEndpoint:
func NewSubscriberEndpoint(topic string, messageHandler mqtt.MessageHandler) MQTTClientSubscriberEndpoint {
	var mqttClientSubscriberEndpoint MQTTClientSubscriberEndpoint
	mqttClientSubscriberEndpoint.Topic = topic
	mqttClientSubscriberEndpoint.MessageHandler = messageHandler
	addSubscriberEndpoint(&mqttClientSubscriberEndpoint)
	return mqttClientSubscriberEndpoint
}

// subscribe to a topic "application_name/element_name"
func subscribe(subscriber MQTTClientSubscriberEndpoint) bool {
	if mqttClient.Status.State == dataModel.CONNECTED {
		logger.Println("subscribe: ", subscriber.Topic, subscriber.MessageHandler)
		topic := subscriber.Topic
		token := mqttClientConnection.Subscribe(topic, 1, subscriber.MessageHandler)
		token.Wait()
		return (token.Error() == nil)
	}
	return false
}

// unsubscribe to a topic "application_name/element_name"
func unsubscribe(subscriber MQTTClientSubscriberEndpoint) bool {
	if mqttClient.Status.State == dataModel.CONNECTED {
		topic := subscriber.Topic
		token := mqttClientConnection.Unsubscribe(topic)
		token.Wait()
		return (token.Error() == nil)
	}
	return false
}

// add subscriber endpoint
func addSubscriberEndpoint(subscriber *MQTTClientSubscriberEndpoint) bool {
	// check if already exists
	for _, s := range subscriberEndpoints {
		if s.Topic == subscriber.Topic {
			return true
		}
	}

	// create the endpoint
	subscriberEndpoints = append(subscriberEndpoints, *subscriber)

	// subscribes if it is connected
	if mqttClientConnection != nil && mqttClientConnection.IsConnected() {
		if subscribe(*subscriber) {
			return true
		}
	}
	return false
}

// remove subscriber endpoint
func removeSubscriberEndpoint(subscriber *MQTTClientSubscriberEndpoint) bool {
	// unsubscribes if it is connected
	if mqttClientConnection != nil && mqttClientConnection.IsConnected() {
		unsubscribe(*subscriber)
	}

	// check if already exists
	var newSubscriberEndpoints []MQTTClientSubscriberEndpoint
	for _, s := range subscriberEndpoints {
		if s.Topic != subscriber.Topic {
			newSubscriberEndpoints = append(newSubscriberEndpoints, s)
		}
	}
	subscriberEndpoints = newSubscriberEndpoints

	return true
}

// SubscriberEndpoint method to close the message channel
func (mqttClientSubscriberEndpoint MQTTClientSubscriberEndpoint) Cancel() {
	removeSubscriberEndpoint(&mqttClientSubscriberEndpoint)
}

// on reconnection - resubscribe the topics already added
func reSubscribe() {
	if mqttClientConnection.IsConnected() {
		for _, sub := range subscriberEndpoints {
			subscribe(sub)
		}
	}
}

// addLog
func addLog(state dataModel.MQTTClientState, error string) {
	var mqttClientLog dataModel.MQTTClientLog
	mqttClientLog.State = state
	mqttClientLog.Error = error
}
