/*
Copyright (c) 2018-2022 Perinet GmbH
All rights reserved

This software is dual-licensed: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation. For the terms of this
license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
or contact us at https://perinet.io/contact/ when you want license
this software under a commercial license.
*/

package mqttclient

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/apiservice/mqttclient/dataModel"
	"gitlab.com/perinet/generic/lib/httpserver/auth"
	"gitlab.com/perinet/generic/lib/utils/intHttp"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/node/nodeErrorStatus"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
)

type Security int

const (
	MQTTS_DEFAULT_PORT = 8883
)

// TODO: take the interface from apiservice "network"
var (
	NET_IF    = "eth0"
	reconnect = false
)

// initiate the attempts to connect
func initConnection() dataModel.MQTTClientState {
	logger.Println("init connection")

	go connectBrokerLoop(&mqttClientConnection, &mqttClientConnectionOptions)
	return mqttClient.Status.State
}

func reinitConnection() {
	logger.Println("reinit connection")

	reconnect = true
}

// Look for the ip address and port given a broker_uri
func findApplicationBroker(application_name string) (string, string, uint16) {
	var data []byte
	var err error
	var vars = map[string]string{}

	// update state
	mqttClient.Status.State = dataModel.SEARCHING
	mqttClient.Status.BrokerURI = ""
	mqttClient.Status.SecurityLevel = dataModel.UNSECURED

	logger.Println("Searching an application broker...")

	// init variables
	hostname := ""
	addr := ""
	var port uint16 = 0

	vars["service_name"] = "_secure-mqtt._tcp"

	// Get the Services available
	data = intHttp.Get(dnssd.DNSSDServiceInfoDiscoverGet, map[string]string{"service_name": "_secure-mqtt._tcp"})

	var service_info []dnssd.DNSSDServiceInfo
	err = json.Unmarshal(data, &service_info)
	if err != nil {
		logger.Println("Error to unmarshal service_info: no services _secure-mqtt._tcp found!")
		return "", "", 0
	}
	
	for _, service := range service_info {
		// define the string to find
		find_application_name_record := "application_name=" + application_name

		switch txtRecord := service.TxtRecord.(type) {
		case []string:
			// TxtRecord is a slice of strings
			for _, record := range txtRecord {
				if (record == find_application_name_record ) {
					
					addr := service.Addresses[0].String()
					port := service.Port
					hostname := service.Hostname

					// Print or return the address, hostname, and port
					fmt.Printf("Found service:\nHostname: %s\nAddress: %s\nPort: %d\n", hostname, addr, port)
					return hostname, addr, port
				}
			}
		default:
			log.Println("Unknown TxtRecord type")
		}
	}

	// return address and port
	return hostname, addr, port
}

// Open the client connection to the MQTT Broker
func openConnection(mqttClientConnection *mqtt.Client, mqttClientConnectionOptions *mqtt.ClientOptions) bool {

	var addr net.IP
	var port uint16 = 0
	var broker_hostname = ""

	logger.Println("openConnection:  init... mqttClient.Status.State: ", mqttClient.Status.State)
	if mqttClient.Status.State != dataModel.CONNECTED && mqttClient.Status.State != dataModel.DISABLED {
		// update to status CONNECTING
		mqttClient.Status.State = dataModel.CONNECTING

		var node_config node.NodeConfig
		var err error
		data := intHttp.Get(node.NodeConfigGet, nil)
		err = json.Unmarshal(data, &node_config)
		if err != nil {
			logger.Println("Failed to fetch NodeConfig: ", err.Error())
			updateNodeStatus(nodeErrorStatus.ERROR)
			return false
		}

		// once the broker is not configured try to connect to find a broker by application name
		if mqttClient.Config.BrokerURI == "" {
			if node_config.ApplicationName != "" {
				var addrString string
				broker_hostname, addrString, port = findApplicationBroker(node_config.ApplicationName)
				addr = net.ParseIP(addrString)
			} else {
				logger.Println("no application name, set to disconnected")
				mqttClient.Status.State = dataModel.DISCONNECTED
				mqttClient.Status.BrokerURI = ""
				mqttClient.Status.SecurityLevel = dataModel.UNSECURED
				updateNodeStatus(nodeErrorStatus.WARNING)
				return false
			}
		} else {
			var host string
			url, err := url.Parse(mqttClient.Config.BrokerURI)
			if err != nil || url.Host == "" {
				logger.Println("failed to parse url", err)
				logger.Println("try to parse as host:port")
				host = mqttClient.Config.BrokerURI
			} else {
				host = url.Host
			}

			var ip net.IP
			hostname, portString, err := net.SplitHostPort(host)
			if err != nil {
				logger.Println("failed to parse as host:port, try to parse without port: ", err)

				ip = net.ParseIP(strings.TrimSuffix(strings.TrimPrefix(host, "["), "]"))

				if ip != nil {
					addr = ip
					port = 0 // use default port
				} else {
					ips, err := net.LookupIP(host)
					if err != nil {
						logger.Println("failed to resolve broker hostname")
						updateNodeStatus(nodeErrorStatus.WARNING)
						return false
					}
					addr = ips[0] // use first ip
					broker_hostname = host
					port = 0 // use default port
				}
			} else {
				addr = net.ParseIP(hostname)
				if addr == nil {
					logger.Println("failed to parse ip")
					updateNodeStatus(nodeErrorStatus.WARNING)
					return false
				}

				port_uint64, err := strconv.ParseUint(portString, 10, 16)
				if err != nil {
					logger.Println("failed to parse port: ", err)
					updateNodeStatus(nodeErrorStatus.WARNING)
					return false
				}
				port = uint16(port_uint64)
			}
		}

		if addr != nil {
			var broker_addr string

			if addr.IsLinkLocalUnicast() {
				// add scope id to link-local addresses
				broker_addr = "[" + addr.String() + "%25" + NET_IF + "]"
			} else {
				broker_addr = "[" + addr.String() + "]"
			}

			tlsConfig := newTLSConfig(broker_hostname, port) // the name here needs to be the hostname set in the certificate!

			// TODO minimum security needs to be implemented
			// if authenticatedisrequired && skipVerify == true {
			// 	addLog(dataModel.FAILED, "mqtt_client: root ca certificate is missing for authenticated connection")
			// 	return false
			// }
			// if mTLSisrequired && tlsConfig.ClientAuth == tls.NoClientCert {
			// 	addLog(dataModel.FAILED, "mqtt_client: client certificate is missing for mTLS connection")
			// 	return false
			// }

			logger.Printf("mqtt tls config %+v\n", tlsConfig)
			mqttClientConnectionOptions.SetTLSConfig(tlsConfig)

			// use default port
			if port == 0 {
				port = MQTTS_DEFAULT_PORT
			}

			mqttClientConnectionOptions.AddBroker(fmt.Sprintf("ssl://%s:%d", broker_addr, port))

			// set client ID
			if node_config.ElementName != "" {
				mqttClientConnectionOptions.SetClientID(node_config.ElementName)
			} else {
				hostname, err := os.Hostname()
				if err != nil {
					addLog(dataModel.FAILED, "Failed to get hostname")
					updateNodeStatus(nodeErrorStatus.WARNING)
					return false
				}
				mqttClientConnectionOptions.SetClientID(hostname)
			}

			// define the callback functions
			mqttClientConnectionOptions.OnConnect = connectHandler
			mqttClientConnectionOptions.OnConnectionLost = connectLostHandler

			// Set the mqtt client connection with the options configured
			*mqttClientConnection = mqtt.NewClient(mqttClientConnectionOptions)
			// try to connect and wait for the token
			if token := (*mqttClientConnection).Connect(); token.Wait() && token.Error() != nil {
				logger.Println(token.Error())
				// update State
				mqttClient.Status.State = dataModel.FAILED
				addLog(dataModel.FAILED, token.Error().Error())
				updateNodeStatus(nodeErrorStatus.ERROR)
				mqttClient.Status.BrokerURI = ""
				mqttClient.Status.SecurityLevel = dataModel.UNSECURED
				return false
			} else {
				// ssl connected
				mqttClient.Status.State = dataModel.CONNECTED
				mqttClient.Status.BrokerURI = broker_hostname + ":" + fmt.Sprint(port)

				// setup the security level used
				if (tlsConfig.ClientAuth == tls.NoClientCert) {
					mqttClient.Status.SecurityLevel = dataModel.ENCRYPTED
				} else {
					mqttClient.Status.SecurityLevel = dataModel.AUTHORIZED
				}
				return true
			}
		} else {
			// failed all attempts
			mqttClient.Status.State = dataModel.FAILED
			mqttClient.Status.BrokerURI = ""
			mqttClient.Status.SecurityLevel = dataModel.UNSECURED

			mqttClient.Status.FailCount = mqttClient.Status.FailCount + 1
			addLog(dataModel.FAILED, "All connection attempts have failed!")
			if mqttClient.Status.FailCount > 10 {
				updateNodeStatus(nodeErrorStatus.WARNING)
			}
			return false
		}
	}
	return true
}

// Connection Handler
var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {

	if (mqttClientConnection.IsConnected()) {
		logger.Println("OnConnectHandler: mqttClientConnection is connected!")
		mqttClient.Status.State = dataModel.CONNECTED
		reSubscribe()
		addLog(dataModel.CONNECTED, "null")
	} else {
		logger.Println("OnConnectHandler: mqttClientConnection is NOT connected!")
	}
}

// Reconnect handler
func reconnectHandler(mqttClientConnection *mqtt.Client, mqttOptions *mqtt.ClientOptions) {
	logger.Println("mqttClient reconnectHandler")

	if *mqttClientConnection != nil {
		logger.Println("mqttCient: reconnectHandler mqttClientConnection not null")

		if (*mqttClientConnection).IsConnected() {
			logger.Println("mqttCient: reconnectHandler disconnecting")
			(*mqttClientConnection).Disconnect(MQTT_WAIT_WORK)

			mqttClient.Status.State = dataModel.DISCONNECTED
			mqttClient.Status.BrokerURI = ""
			mqttClient.Status.SecurityLevel = dataModel.UNSECURED
		} else {
			mqttClient.Status.State = dataModel.DISCONNECTED
			mqttClient.Status.BrokerURI = ""
			mqttClient.Status.SecurityLevel = dataModel.UNSECURED
		}
	}
	logger.Println("mqttCient: reconnectHandler openConnection..")
	openConnection(mqttClientConnection, mqttOptions)
}

// Handler connection lost callback
var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	logger.Println("Connection lost: ", string(err.Error()))
	
	mqttClient.Status.State = dataModel.DISCONNECTED
	mqttClient.Status.BrokerURI = ""
	mqttClient.Status.SecurityLevel = dataModel.UNSECURED

	addLog(dataModel.DISCONNECTED, string(err.Error()))
	if client.IsConnected() {
		logger.Println("client disconnecting... ")
		client.Disconnect(MQTT_WAIT_WORK)
	}

	// try to reconnect
	if !mqttClient.Config.Disabled {
		reconnect = true
	}
}

// create the tls configuration object
func newTLSConfig(broker string, port uint16) *tls.Config {

	clientAuth := tls.RequireAndVerifyClientCert
	serverName := broker
	certpool := x509.NewCertPool()
	certs := []tls.Certificate{}
	pemCerts := intHttp.Get(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"})

	skipVerify := true
	verifyPeerCertificateHandler := func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
		var certs []*x509.Certificate
		for _, rawCert := range rawCerts {
			cert, err := x509.ParseCertificate(rawCert)
			if err != nil {
				logger.Println("verifyPeerCertificateHandler parse err: ", err)
				updateNodeStatus(nodeErrorStatus.FATAL)
			} else if cert != nil {
				certs = append(certs, cert)
			}
		}

		err := auth.VerifyCertificatesWithoutRole(certs)
		if err != nil {
			logger.Println("verifyPeerCertificateHandler verify err: ", err)

			mqttClient.Status.SecurityLevel = dataModel.ENCRYPTED
			updateNodeStatus(nodeErrorStatus.FATAL)
		} else {
			
			mqttClient.Status.SecurityLevel = dataModel.AUTHORIZED
		}
		
		// update broker_uri
		if port == 0 {
			port = MQTTS_DEFAULT_PORT
		}
		mqttClient.Status.BrokerURI = broker + ":" + fmt.Sprint(port)

		return nil
	}

	if pemCerts != nil {
		skipVerify = false
		certpool.AppendCertsFromPEM(pemCerts)
		client_cert := intHttp.Get(security.Security_Certificate_Get, map[string]string{"certificate": "client_cert"})
		client_key := intHttp.Get(security.Security_Certificate_Key_Get, map[string]string{"certificate": "client_cert"})
		cert, err := tls.X509KeyPair(client_cert, client_key)
		if client_cert == nil || client_key == nil || err != nil {
			clientAuth = tls.NoClientCert
		} else {
			cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
			if err != nil {
				clientAuth = tls.NoClientCert
			} else {
				certs = append(certs, cert)
			}
		}
	} else {
		clientAuth = tls.NoClientCert
	}

	// return the object
	return &tls.Config{
		RootCAs:               certpool,
		ServerName:            serverName,
		ClientAuth:            clientAuth,
		ClientCAs:             certpool,
		InsecureSkipVerify:    skipVerify,
		VerifyPeerCertificate: verifyPeerCertificateHandler,
		Certificates:          certs,
	}
}

// get the file time
func getModTime(fileName string) time.Time {
	file, err := os.Stat(fileName)
	if err != nil {
		return time.Time{}
	}
	return file.ModTime()
}

// loop to manage the connection
func connectBrokerLoop(mqttClient *mqtt.Client, mqttOptions *mqtt.ClientOptions) {
	certTimeStamps := [2]time.Time{getModTime(security.ROOT_CA_CERT), getModTime(security.CLIENT_CERT)}

	for {
		if *mqttClient != nil {
			if !(*mqttClient).IsConnected() {
				openConnection(mqttClient, mqttOptions)
			}
		} else {
			logger.Print("nil open")
			openConnection(mqttClient, mqttOptions)
		}
		time.Sleep(2000 * time.Millisecond)
		for i, f := range []string{security.ROOT_CA_CERT, security.CLIENT_CERT} {
			curTimeStamp := getModTime(f)
			if certTimeStamps[i] != curTimeStamp {
				reconnectHandler(mqttClient, mqttOptions)
			}
			certTimeStamps[i] = curTimeStamp
		}
		if reconnect {
			reconnect = false
			reconnectHandler(mqttClient, mqttOptions)
		}
	}
}
